var session = layui.sessionData('cate');
var user = session.user;//客户端用户信息

//保存会话信息
function SaveSession(userinfo) {
    layui.sessionData('cate', {
        key: 'user',
        value: userinfo
    });
}

//清空会话信息
function RemoveSession() {
    layui.sessionData('cate', {
        key: 'user',
        remove: true
    });
}

//重新登录
function ReLogin() {
    RemoveSession();//清空会话信息
    top.location.href = "login.html";
}