/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80019
 Source Host           : localhost:3306
 Source Schema         : fp_docmanager

 Target Server Type    : MySQL
 Target Server Version : 80019
 File Encoding         : 65001

 Date: 09/01/2022 21:40:24
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for fp_appinfo
-- ----------------------------
DROP TABLE IF EXISTS `fp_appinfo`;
CREATE TABLE `fp_appinfo`  (
  `id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ID',
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `apppath` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `author` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `appkey` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `intro` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `version` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `target` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `adminindex` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `apptype` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '是否系统应用',
  `datetime` datetime(0) DEFAULT NULL,
  `updatetime` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fp_appinfo
-- ----------------------------
INSERT INTO `fp_appinfo` VALUES ('14819286672786432', '方配后台管理系统', '/admin/', '方配', 'fpadmin.png', 'FPAdmin', '提供方配.Core后台管理系统，官方网站：http://www.fangpage.com', '1.0.0', '', '', '1', '2021-04-18 16:00:14', '2021-10-25 14:18:04');
INSERT INTO `fp_appinfo` VALUES ('14819288422794240', '系统用户管理', '/user/', '方配', '', 'FPUser', '提供方配.Core后台用户管理，官方网站：http://www.fangpage.com', '1.0.0', '', '', '1', '2021-04-19 13:07:58', '2021-06-08 09:24:36');
INSERT INTO `fp_appinfo` VALUES ('15322740856259584', '验证码插件', '/plugins/verify/', '方配', '', 'FPVerify', '提供在应用中使用验证码功能。', '1.0.0', '', 'verify', '2', '2021-06-08 08:58:03', '2021-06-08 10:57:58');
INSERT INTO `fp_appinfo` VALUES ('15345878991946752', 'Froala富文本编辑器', '/plugins/froala_editor/', 'Froala', '', 'FroalaEditor', '富文本编辑器', '1.0.0', '', 'https://froala.com/wysiwyg-editor', '2', '2021-06-24 17:15:23', '2021-06-25 13:53:29');
INSERT INTO `fp_appinfo` VALUES ('15346034237195264', 'Font Awesome', '/plugins/font-awesome/', 'FontAwesome', '', 'FontAwesome', '一套绝佳的图标字体库和CSS框架', '4.7.0', '', 'https://fontawesome.dashgame.com', '2', '2021-06-24 19:53:19', '2021-06-25 12:05:37');
INSERT INTO `fp_appinfo` VALUES ('15346050112472064', 'Animate', '/plugins/animate/', 'Animate', '', 'Animate', '强大的跨平台的预设css3动画库', '4.1.1', '', 'http://www.animate.net.cn', '2', '2021-06-24 20:09:27', '2021-06-25 13:52:42');
INSERT INTO `fp_appinfo` VALUES ('15625089362625536', '方配文档管理', '/doc/', '方配', '', 'FPDoc', '提供公文管理系统', '1.0.0', '', '', 'user', '2022-01-07 23:02:52', '2022-01-07 23:02:52');
INSERT INTO `fp_appinfo` VALUES ('15625095628243968', '类型管理', '/type/', '方配', '', 'FPType', '', '1.0.0', '', '', 'user', '2022-01-07 23:09:14', '2022-01-07 23:09:14');
INSERT INTO `fp_appinfo` VALUES ('15627745028785152', 'PDF浏览工具', '/fppdf/', '方配', '', 'FPPDF', '提供Word转换PDF在线浏览', '1.0.0', '', '', 'user', '2022-01-09 20:04:21', '2022-01-09 20:05:27');

-- ----------------------------
-- Table structure for fp_department
-- ----------------------------
DROP TABLE IF EXISTS `fp_department`;
CREATE TABLE `fp_department`  (
  `id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `parentid` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `shortname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `depth` int(0) DEFAULT NULL,
  `display` int(0) DEFAULT NULL,
  `status` tinyint(0) DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fp_department
-- ----------------------------
INSERT INTO `fp_department` VALUES ('14819286672786432', '', '研发部', '', 0, 1, 0);
INSERT INTO `fp_department` VALUES ('14819288422794240', '14819286672786432', '开发部', '', 0, 1, 0);
INSERT INTO `fp_department` VALUES ('14819288578327552', '14819286672786432', '测试部', '', 0, 2, 0);

-- ----------------------------
-- Table structure for fp_docinfo
-- ----------------------------
DROP TABLE IF EXISTS `fp_docinfo`;
CREATE TABLE `fp_docinfo`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `title` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `number` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `doctype` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `orgtype` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `yeartype` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `yeartype_name` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `docfile` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `filetype` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `datetime` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fp_docinfo
-- ----------------------------
INSERT INTO `fp_docinfo` VALUES (11, '软件授权书', '2020[1]号', '15625117296247808', '15625118947378176', '15627546260816896', '2020', '/upload/202201/09/202201092135287592.docx', '.docx', '2022-01-09 21:35:55');
INSERT INTO `fp_docinfo` VALUES (12, '配伍题(1)', '2020【2】号', '15625117743989760', '15625119507514368', '15625119897076736', '2021', '/upload/202201/09/202201092136415135.docx', '.docx', '2022-01-09 21:37:00');

-- ----------------------------
-- Table structure for fp_menuinfo
-- ----------------------------
DROP TABLE IF EXISTS `fp_menuinfo`;
CREATE TABLE `fp_menuinfo`  (
  `id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `parentid` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `markup` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `depth` int(0) DEFAULT NULL,
  `display` int(0) DEFAULT NULL,
  `status` tinyint(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fp_menuinfo
-- ----------------------------
INSERT INTO `fp_menuinfo` VALUES ('14816339144819712', '15555233462535168', '主页', 'homeindex', 'layui-icon layui-icon-home', 'sys_config.html', 0, 1, 1);
INSERT INTO `fp_menuinfo` VALUES ('14817755636073472', '15555233462535168', '系统设置', '', 'layui-icon layui-icon-set', '', 0, 2, 1);
INSERT INTO `fp_menuinfo` VALUES ('14817838928593920', '15555233462535168', '用户设置', '', 'layui-icon layui-icon-user', '', 0, 3, 1);
INSERT INTO `fp_menuinfo` VALUES ('14817847193945088', '14817755636073472', '菜单管理', NULL, '', '/admin/menu_list.html', 0, 3, 1);
INSERT INTO `fp_menuinfo` VALUES ('14817878472803328', '14817838928593920', '角色管理', NULL, '', '/user/role_list.html', 0, 2, 1);
INSERT INTO `fp_menuinfo` VALUES ('14819214596637696', '14817838928593920', '部门管理', NULL, '', '/user/depart_list.html', 0, 1, 1);
INSERT INTO `fp_menuinfo` VALUES ('14819291942732800', '14817838928593920', '用户管理', NULL, '', '/user/user_list.html', 0, 4, 1);
INSERT INTO `fp_menuinfo` VALUES ('14821765541135360', '14817755636073472', '应用管理', NULL, '', '/admin/app_manage.html', 0, 2, 1);
INSERT INTO `fp_menuinfo` VALUES ('14829225151841281', '14817755636073472', '通用设置', NULL, '', '/admin/sys_config.html', 0, 1, 1);
INSERT INTO `fp_menuinfo` VALUES ('15555233462535168', '', '系统管理', '', 'layui-icon layui-icon-set', '', 0, 1, 1);
INSERT INTO `fp_menuinfo` VALUES ('15625096661697536', '', '文档管理', '', 'layui-icon layui-icon-app', '', 0, 2, 1);
INSERT INTO `fp_menuinfo` VALUES ('15625099811111936', '15625096661697536', '文档类型管理', '', 'layui-icon layui-icon-app', '/type/type_list.html', 0, 1, 1);
INSERT INTO `fp_menuinfo` VALUES ('15625124724720640', '15625096661697536', '发文文档管理', '', 'layui-icon layui-icon-home', '/doc/doc_list.html', 0, 2, 1);

-- ----------------------------
-- Table structure for fp_roleinfo
-- ----------------------------
DROP TABLE IF EXISTS `fp_roleinfo`;
CREATE TABLE `fp_roleinfo`  (
  `id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `detail` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `isadmin` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `departs` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `menus` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `status` tinyint(0) DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fp_roleinfo
-- ----------------------------
INSERT INTO `fp_roleinfo` VALUES ('14819199099962368', '超级管理员', 'super', '', '1', '14819286672786432,14819288578327552', '15555233462535168,14816339144819712,14817755636073472,14829225151841281,14821765541135360,14817847193945088,14817838928593920,14819214596637696,14817878472803328,14819291942732800,15625096661697536,15625099811111936,15625124724720640,15625126728680448', 1);

-- ----------------------------
-- Table structure for fp_typeinfo
-- ----------------------------
DROP TABLE IF EXISTS `fp_typeinfo`;
CREATE TABLE `fp_typeinfo`  (
  `id` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `parentid` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `typename` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `markup` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `note` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `display` int(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fp_typeinfo
-- ----------------------------
INSERT INTO `fp_typeinfo` VALUES ('15625112654103552', '', '文档类别', 'doctype', '', 1);
INSERT INTO `fp_typeinfo` VALUES ('15625114854294528', '', '发布机关', 'orgtype', '', 2);
INSERT INTO `fp_typeinfo` VALUES ('15625116384609280', '', '发布年份', 'yeartype', '', 3);
INSERT INTO `fp_typeinfo` VALUES ('15625117296247808', '15625112654103552', '报告决议', '', '', 1);
INSERT INTO `fp_typeinfo` VALUES ('15625117743989760', '15625112654103552', '计划刚要', '', '', 2);
INSERT INTO `fp_typeinfo` VALUES ('15625118290150400', '15625112654103552', '法律法规', '', '', 3);
INSERT INTO `fp_typeinfo` VALUES ('15625118554260480', '15625112654103552', '标准', '', '', 4);
INSERT INTO `fp_typeinfo` VALUES ('15625118947378176', '15625114854294528', '中共中央', '', '', 1);
INSERT INTO `fp_typeinfo` VALUES ('15625119507514368', '15625114854294528', '中共中央办公厅', '', '', 2);
INSERT INTO `fp_typeinfo` VALUES ('15625119897076736', '15625116384609280', '2021', '', '', 1);
INSERT INTO `fp_typeinfo` VALUES ('15625120017269760', '15625116384609280', '2022', '', '', 2);
INSERT INTO `fp_typeinfo` VALUES ('15627546260816896', '15625116384609280', '2020', '', '', 3);
INSERT INTO `fp_typeinfo` VALUES ('15627722389095424', '15625116384609280', '2009', '', '', 4);

-- ----------------------------
-- Table structure for fp_userinfo
-- ----------------------------
DROP TABLE IF EXISTS `fp_userinfo`;
CREATE TABLE `fp_userinfo`  (
  `id` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `headimg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `realname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `gender` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `mobile` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `pid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `roleid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `departid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `openid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `status` tinyint(0) DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fp_userinfo
-- ----------------------------
INSERT INTO `fp_userinfo` VALUES ('14820260937352192', 'admin', 'e10adc3949ba59abbe56e057f20f883e', NULL, '管理员', '男', '13481092810', 'fangpage@foxmail.com', 'q', '14819199099962368', '14819288422794240', '0', '', 1);

SET FOREIGN_KEY_CHECKS = 1;
